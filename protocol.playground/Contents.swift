import Foundation

//Реализовать структуру IOSCollection и создать в ней copy on write по типу - https://www.youtube.com/watch?v=QsoqHGgX2rE&t=594s
struct IOSCollection {
    var changelog: String
}

class Reference <T> {
    var value: T
    init(value: T) {
        self.value = value
    }
}

struct Case <T> {
    var ref: Reference <T>
    init(value: T) {
        self.ref = Reference(value: value)
    }
    var value: T {
        get {
            ref.value
        }
        set {
            guard (isKnownUniquelyReferenced(&ref)) else {
                ref = Reference(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}

//Создать протокол Hotel с инициализатором который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол.
protocol Hotel {
    var roomCount : Int {get}
}

class HotelAlfa : Hotel {
    var rooms : Int = 0
    var roomCount: Int {
        return rooms
    }
}

//Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так что б когда мы напишем такую конструкцию "let diceCoub = 4 \n diceCoub.numberDice" в консоле мы увидели такую строку - "Выпало 4 на кубике".
protocol GameDice {
    var numberDice : String {get}
}
extension Int : GameDice {
    var numberDice : String {
        return("Выпало \(self) на кубике")
    }
}

let diceCoub = 4
print(diceCoub.numberDice)

//Создать протокол с одним методом и 2 мя свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство.
@objc protocol example {
    var text : String {get}
    @objc optional var number: Int {get}
    
    func printer()
}

class ExpampleClass : example {
    var text : String
    init (text: String) {
        self.text = text
    }
    func printer() {
        print(text)
    }
}
let exp = ExpampleClass(text: "Sample text")
exp.printer()

/* Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding().
 Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)
 Компании подключаем два этих протокола
 Задача: вывести в консоль сообщения - 'разработка началась. пишем код <такой-то>' и 'работа закончена. Сдаю в тестирование', попробуйте обработать крайние случаи.*/

protocol StartCode {
    var time : Int {get}
    var codeStrings : Int {get}
    
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol StopCode {
    func stopCoding()
}

enum Platform : String {
    case ios
    case android
    case web
}

class Company : StartCode, StopCode {
    
    var numberOfProgrammers: Int
    var specialties : Platform
    var codeStrings : Int = 0
    var time : Int = 0
    
    init(numberOfProgrammers: Int, specialties : Platform)  {
        self.numberOfProgrammers = numberOfProgrammers
        self.specialties = specialties
    }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        guard platform == specialties else {
                    print("Нет программистов, специализирующихся на \(platform).")
                    return
                }
                
                guard numberOfSpecialist <= numberOfProgrammers else {
                    print("Недостаточно программистов для работы.")
                    return
                }
        print("Разработка началась. пишем код.")
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование.")
    }
    
}

let company = Company(numberOfProgrammers: 8, specialties: .ios)
company.writeCode(platform: .android, numberOfSpecialist: 2)
company.writeCode(platform: .ios, numberOfSpecialist: 10)
company.writeCode(platform: .ios, numberOfSpecialist: 6)
company.stopCoding()
